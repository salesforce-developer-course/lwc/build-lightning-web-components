public with sharing class ContactController {
    @AuraEnabled(cacheable=true) //When an @AuraEnabled method is cacheable, Data Manipulation Language (DML) operations are not allowed
    /* public static List<Contact> getContactsBornAfter(Date birthDate){
        return [
            SELECT Name, Title, Email, Phone
            FROM Contact
            WHERE Birthdate > :birthDate
            WITH SECURITY_ENFORCED
        ];
    }
    */
    public static List<Contact> getContacts() {
        return [
            SELECT FirstName, LastName, Email
            FROM Contact
            WITH SECURITY_ENFORCED
            ORDER BY FirstName
        ];
    }
}